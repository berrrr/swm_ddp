﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO.Ports;
using System.Windows.Threading;

using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.FaceTracking;
using Point = System.Windows.Point;
using OpenCvSharp;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Windows.Controls.Primitives;

namespace 통합프로젝트_1
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        bool isRunning = false;
        int Frames = 15;
        //현재 단계  1 : 전후진 판단   2 : 전조 판단   3 : 성숙 판단 4 : 졸음. ->경고
        int now_phase = 1;


        #region 비프음 준비

        [System.Runtime.InteropServices.DllImport("KERNEL32.DLL")]  //비프음dll
        extern public static void Beep(int freq, int dur);           //비프음 함수
        Thread BeepThread;
        #endregion

        #region gps 포트 정보
        //gps 포트 정보
        SerialPort port;
        DispatcherTimer dTimer;
        double[,] pastGeo;
        #endregion

        //키넥트 센서
        KinectSensor nui = null;

        #region 이미지포맷
        //이미지포맷
        private byte[] colorImage;
        private ColorImageFormat colorImageFormat = ColorImageFormat.Undefined;
        private short[] depthImage;
        private DepthImageFormat depthImageFormat = DepthImageFormat.Undefined;
        private Skeleton[] skeletonData;
        private FaceTracker faceTracker;
        private EnumIndexableCollection<FeaturePoint, PointF> facePoints;
        #endregion

        #region 시간정보
        //시간정보
        long m_saveFrame = 0;
        long m_saveTime = 0;
        long m_nowTime = 0;
        #endregion


        #region 입술
        private double lip_aboveX, lip_aboveY, lip_bottomX, lip_bottomY;
        private int open_sec;
        int min_lip = 9999;
        bool open_flag; // 하품이 1분 내에 있었는지 판단 -> 하품 검사가 있으면  true  1분내에 없으면 false
        int frame_sec; // 프레임이 몇번 지났는지 체크(첫번째 하품 이후 1초 내에 하품이 있으면 다음 페이즈로)

        #endregion

        #region 각도

        float noseX = 0;
        float noseY = 0;
        float chinX = 0;
        float chinY = 0;

        Line lineOfHeadNose = new Line();
        Line lineOfChinNeck = new Line();

        public struct HeadNod
        {
            public bool startNod;
            public bool endNod;
            public int nodCount;
            public int nodTime;
        }
        HeadNod checkHeadNod = new HeadNod();

        #endregion

        #region 눈
        // 눈 정보
        bool isEyesDetected;

        int eyes_X, eyes_Y, eyes_width, eyes_height;
        CvRect eye_rect = new CvRect(); // 눈 사각형 저장

        /////////// 정면주시 여부 정보 ////////////
        IplImage eye_template;
        IplImage eye_templateSrc;
        IplImage eye_templateRes;
        bool isTempleteMatch = false;
        bool startTempleteMatch = false;
        short match_frame = 0;
        short match_cnt = 0;


        /////////// 눈 깜빡임 정보 ////////////
        bool blink_flag;      //3프레임 내에 눈을 감았는지 검사
        short blink_frame = 0;  // 눈을 감은 후 몇 프레임이 지났나 short로 한건 용량(속도)를 조금이라도 줄이기 위해서
        short blink_cnt = 0;    // 눈 깜빡임 횟수


        /////////// 눈 감김 정도 정보 ////////////
        int eye_frames = 0; //시간계산
        int[] eye_hist = new int[100]; // 히스토그램 값 배열
        int[] eye_average = new int[100]; //히스토그램 평균 값
        int average_max = 0, average_min = 99999;  //평균 크기의 큰 값, 작은 값
        int average_cnt = 0; //평균 구한 횟수

        #endregion

        public MainWindow()
        {
            InitializeComponent();
            announce_gps();
            //announce_angle();

        }


        #region gps 소스

        private void dTimer_Tick(object sender, EventArgs e)
        {
            string buff = string.Empty;
            buff = port.ReadExisting();
            Console.WriteLine(buff);

            string[] gpsEx = buff.Split('$');

            for (int i = 0; i < gpsEx.Length; i++)
            {
                if (gpsEx[i].Split(',')[0] == "GPGGA")
                {
                    string[] data = gpsEx[i].Split(',');
                    //[1] = UTC
                    //[2] = 위도
                    //[4] = 경도
                    //[6] = Fix
                    //[7] = 사용된 위a성 수 
                    //[9] = 해수면 고도
                    if (Int32.Parse(data[6]) > 0)
                    {
                        guessMove(data[2], data[4]);

                        Console.WriteLine(gpsEx[i]);
                        Console.WriteLine("{0} / {1} / {2} / {3} / {4} / {5}", data[1], data[2], data[4], data[6], data[7], data[9]);
                        pastGeo[0, 0] = getCoordinate(data[2]);
                        pastGeo[0, 1] = getCoordinate(data[4]);

                    }
                }
                else if (gpsEx[i].Split(',')[0] == "GPVTG")
                {
                    string[] data = gpsEx[i].Split(',');
                    if (Convert.ToInt32(data[7]) >= 10.0)
                    {
                        imgDrive.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Car/grey_car_forward.jpg"));
                        ChangePhase(2);
                    }
                    else
                    {
                        imgDrive.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Car/car_parking.jpg"));
                        ChangePhase(1);
                    }
                }
            }
        }

        private void guessMove(string Latitude, string Longitude)
        {
            StringBuilder sb = new StringBuilder("이동 방향 : ");
            if (Math.Round(getCoordinate(Latitude), 3) > pastGeo[0, 0])
            {
                sb.Append("북");
            }
            else if (Math.Round(getCoordinate(Latitude), 3) < pastGeo[0, 0])
            {
                sb.Append("남");
            }

            if (Math.Round(getCoordinate(Longitude), 3) > pastGeo[0, 1])
            {
                sb.Append("동");
            }
            else if (Math.Round(getCoordinate(Longitude), 3) < pastGeo[0, 1])
            {
                sb.Append("서");

            }
        }

        private double getCoordinate(string data)
        {
            double result;
            int cutLength = 0;
            if (data.Split('.')[0].Length == 4)
            {
                cutLength = 2;
            }
            else if (data.Split('.')[0].Length == 5)
            {
                cutLength = 3;
            }

            char[] charArray = data.ToCharArray();
            string plusData = String.Empty;
            string divideData = String.Empty;
            for (int i = 0; i < charArray.Length; i++)
            {
                if (i < cutLength)
                {
                    plusData += charArray[i];
                }
                else
                {
                    divideData += charArray[i];
                }
            }
            result = double.Parse(plusData) + (double.Parse(divideData) / 60);
            return result;
        }

        #endregion

        #region 키넥트 소스


        private void pase1_advance_Checked(object sender, RoutedEventArgs e)
        {
            Kinect_Start();
        }

        private void Kinect_Start()
        {
            //try
            //{

            //    if (port != null && port.IsOpen) return;

            //    foreach (string portname in SerialPort.GetPortNames())
            //    {
            //        port = new SerialPort("COM3", 9600);

            //        port.Open();
            //        dTimer.Start();
            //        imgAlert.Visibility = System.Windows.Visibility.Hidden;
            //        return;
            //    }
            //}
            //catch
            //{
            //    imgAlert.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Alert/notice_gps.png"));
            //    imgAlert.Visibility = System.Windows.Visibility.Visible;

            //}

            imgDrive.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Car/grey_car_forward.jpg"));
            ChangePhase(2);

            nui = KinectSensor.KinectSensors[0];

            nui.ColorStream.Enable();

            nui.DepthStream.Enable();
            nui.SkeletonStream.Enable();
            nui.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
            nui.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(nui_AllFramesReady);

            nui.Start();
        }

        bool frame_flag = false;

        private void nui_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            ColorImageFrame colorImageFrame = null;
            DepthImageFrame depthImageFrame = null;
            SkeletonFrame skeletonFrame = null;
            BitmapSource src = null;

            if (frame_flag == true)
            {
                frame_flag = false;
                return;
            }
            frame_flag = true;

            try
            {

                colorImageFrame = e.OpenColorImageFrame();
                depthImageFrame = e.OpenDepthImageFrame();
                skeletonFrame = e.OpenSkeletonFrame();
                nui.DepthStream.Range = DepthRange.Near;
                nui.SkeletonStream.EnableTrackingInNearRange = true;

                if (colorImageFrame == null || depthImageFrame == null || skeletonFrame == null)
                {
                    return;
                }

                if (this.depthImageFormat != depthImageFrame.Format)
                {
                    this.depthImage = null;
                    this.depthImageFormat = depthImageFrame.Format;

                }

                if (this.colorImageFormat != colorImageFrame.Format)
                {
                    this.colorImage = null;
                    this.colorImageFormat = colorImageFrame.Format;
                }

                if (this.depthImage == null)
                {
                    this.depthImage = new short[depthImageFrame.PixelDataLength];
                }

                if (this.colorImage == null)
                {
                    this.colorImage = new byte[colorImageFrame.PixelDataLength];
                }

                if (this.skeletonData == null || this.skeletonData.Length != skeletonFrame.SkeletonArrayLength)
                {
                    this.skeletonData = new Skeleton[skeletonFrame.SkeletonArrayLength];
                }

                colorImageFrame.CopyPixelDataTo(this.colorImage);
                depthImageFrame.CopyPixelDataTo(this.depthImage);
                skeletonFrame.CopySkeletonDataTo(this.skeletonData);
            }
            finally
            {
                if (colorImageFrame != null)
                {
                    colorImageFrame.Dispose();
                }

                if (depthImageFrame != null)
                {
                    depthImageFrame.Dispose();
                }

                if (skeletonFrame != null)
                {
                    skeletonFrame.Dispose();
                }
            }

            using (colorImageFrame = e.OpenColorImageFrame())
            {
                if (colorImageFrame != null)
                {
                    try
                    {

                        src = BitmapSource.Create(colorImageFrame.Width, colorImageFrame.Height,
                                            96, 96, PixelFormats.Bgr32, null,
                                            colorImage,
                                            colorImageFrame.Width * colorImageFrame.BytesPerPixel);


                        image1.Source = src;
                    }
                    catch
                    {
                    }
                }
            }

            int m_TrackingId = 0;

            using (depthImageFrame = e.OpenDepthImageFrame())
            {
                if (skeletonFrame != null)
                {
                    bool bFind = false;

                    ////////////// 스켈레톤 중복처리 방지 ////////////////
                    foreach (Skeleton skeleton in skeletonData)
                    {
                        if (skeleton.TrackingState == SkeletonTrackingState.Tracked || skeleton.TrackingState == SkeletonTrackingState.PositionOnly)
                        {
                            if (m_TrackingId == skeleton.TrackingId)
                            {
                                bFind = true;
                                break;
                            }
                        }
                    }

                    if (bFind == false)
                    {
                        m_TrackingId = 0;
                    }

                    foreach (Skeleton skeleton in skeletonData)
                    {
                        if (skeleton.TrackingState == SkeletonTrackingState.Tracked || skeleton.TrackingState == SkeletonTrackingState.PositionOnly)
                        {
                            imgAlert.Visibility = Visibility.Hidden;
                            ////////////// 기존 스켈레톤만 처리 ////////////////
                            if (m_TrackingId != skeleton.TrackingId && m_TrackingId != 0)
                            {
                                continue;
                            }

                            m_TrackingId = skeleton.TrackingId;

                            textblock3.Text = "스켈레톤 중";
                            System.Windows.Point headPoint = new System.Windows.Point();
                            System.Windows.Point neckPoint = new System.Windows.Point();

                            if (now_phase == 2 || now_phase == 3)
                            {
                                if (depthImageFrame != null)
                                {
                                    headPoint = find_head(skeleton, depthImageFrame);
                                    neckPoint = find_neck(skeleton, depthImageFrame);
                                }
                            }

                            if (this.faceTracker == null)
                            {
                                try
                                {
                                    this.faceTracker = new FaceTracker(nui);
                                }
                                catch (InvalidOperationException)
                                {
                                    this.faceTracker = null;
                                }
                            }

                            if (this.faceTracker != null)
                            {
                                FaceTrackFrame frame = this.faceTracker.Track(
                                    colorImageFormat, colorImage, depthImageFormat, depthImage, skeleton);

                                if (frame.TrackSuccessful)
                                {
                                    facePoints = frame.GetProjected3DShape();

                                    find_eyes();
                                    find_lip();
                                    find_nose();
                                    find_chin();

                                }
                                else
                                {
                                    imgAlert.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Alert/notice_facechk.png"));
                                    imgAlert.Visibility = Visibility.Visible;
                                }
                            }

                            if (now_phase == 2)
                            {
                                set_lip();
                                calc_lip();
                            }

                            if (now_phase == 2 || now_phase == 3)
                            {
                                check_nod(headPoint, neckPoint);
                            }

                            /////////////눈 영상만 따오기/////////////////
                            if (src != null)
                            {
                                if (eyes_X > 0 && eyes_Y > 0 && eyes_width > 0 && eyes_height > 0)
                                {
                                    CroppedBitmap eyes_croppedbitmap = eyes_croppedbitmap = new CroppedBitmap(src, new Int32Rect(eyes_X, eyes_Y, eyes_width / 2, eyes_height));

                                    if (depthImageFrame != null)
                                    {
                                        if (facePoints[38].X > find_head(skeleton, depthImageFrame).X)
                                        {
                                            eyes_croppedbitmap = new CroppedBitmap(src, new Int32Rect(eyes_X + (eyes_width / 2), eyes_Y, eyes_width / 2, eyes_height));
                                        }
                                        BitmapSource eyes_bitmapsource = new TransformedBitmap(eyes_croppedbitmap, new ScaleTransform(2, 2));   // 2배 확대한 이미지 출력
                                        image_eyes.Source = eyes_bitmapsource;

                                        check_blink(BitmapConverter.ToIplImage(BitmapSource2Bitmap(eyes_croppedbitmap)));
                                    }

                                }
                            }
                        }

                    }
                }
                else
                {
                    imgAlert.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Alert/notice_notfound.png"));
                    imgAlert.Visibility = Visibility.Visible;
                }
            }

            SetFrameTime(depthImageFrame);
        }

        /////////////타이머/////////////////
        void SetFrameTime(DepthImageFrame depthFrame)
        {
            if (depthFrame == null)
                return;

            long lFrame = (long)depthFrame.FrameNumber;
            long lTime = (long)depthFrame.Timestamp;

            // 1초단위로 검사
            if (lTime - m_saveTime > 1000)
            {
                m_saveFrame = lFrame;
                m_saveTime = lTime;
                m_nowTime++;

                ///////////////////// 졸음 전조 판단 ////////////////////////
                if (now_phase == 2)
                {
                    //////////////// 고개의 끄덕임 횟수 판단 ////////////////
                    if (checkHeadNod.nodCount > 2)
                    {
                        Debug.WriteLine("3 졸음의 성숙단계로 넘어갑니다. ");
                        imgPhaseTwo.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Head/grey_head_nod.jpg"));
                        ChangePhase(3);

                        Thread BeepThread = new Thread(new ThreadStart(call_PreBeep)); // 예비신호 
                        BeepThread.Start();

                        // 고개각도체크 구조체 초기화
                        checkHeadNod.startNod = false;
                        checkHeadNod.endNod = false;
                        checkHeadNod.nodCount = 0;
                        checkHeadNod.nodTime = 0;
                    }

                    // 3분 단위로
                    if (m_nowTime % 180 == 0)
                    {
                        Debug.WriteLine("3분 경과");

                        //고개각도체크 구조체 초기화
                        checkHeadNod.startNod = false;
                        checkHeadNod.endNod = false;
                        checkHeadNod.nodCount = 0;
                        checkHeadNod.nodTime = 0;

                    }

                    //////////////// 눈을 1분 내 2회 이상 깜빡이면 졸음의 전조 ////////////////
                    if (blink_cnt > 1)
                    {
                        Debug.WriteLine("4 졸음의 성숙단계로 넘어갑니다. ");
                        imgPhaseTwo.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Eye/grey_eye_oftenclose.jpg"));
                        ChangePhase(3);
                        blink_cnt = 0;

                        Thread BeepThread = new Thread(new ThreadStart(call_PreBeep)); // 예비신호 
                        BeepThread.Start();
                    }

                    // 1분 단위로 
                    if (m_nowTime % 60 == 0)
                    {
                        Debug.WriteLine("1분 경과");

                        //깜빡임 횟수 초기화
                        blink_cnt = 0;
                    }

                }

                ///////////////////// 졸음 성숙 판단 ////////////////////////
                if (now_phase == 3)
                {
                    // 고개 숙임 지속시간 판단
                    if (checkHeadNod.startNod && !checkHeadNod.endNod)
                    {
                        checkHeadNod.nodTime++;
                        imgPhaseThree.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Head/head_keepdown.jpg"));

                        if (checkHeadNod.nodTime > 3)
                        {
                            checkHeadNod.nodTime = 0;
                            imgPhaseThree.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Head/grey_head_keepdown.jpg"));
                            ChangePhase(4);

                            // 3초 이상이면 경고음
                            Thread BeepThread = new Thread(new ThreadStart(call_OverBeep)); //졸음의 성숙 신호
                            BeepThread.Start();

                        }
                    }



                    //////////////// 눈을 4초 이상 다른곳을 보면 졸음의 실행 ////////////////
                    if (match_cnt > 3)
                    {
                        Debug.WriteLine("5 졸음의 성숙단계로 넘어갑니다. ");
                        imgPhaseThree.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Eye/grey_eye_notfront.jpg"));

                        ChangePhase(4);
                        match_cnt = 0;

                        Thread BeepThread = new Thread(new ThreadStart(call_OverBeep)); // 예비신호 
                        BeepThread.Start();
                    }
                }
            }

        }

        /////////////////입 찾기/////////////////
        private void find_lip()
        {
            // facepoints[5] = 코끝

            lip_aboveX = facePoints[5].X;
            lip_aboveY = facePoints[5].Y;
            lip_bottomX = facePoints[8].X;
            lip_bottomY = facePoints[8].Y;

            if (min_lip > Math.Abs((int)(lip_aboveY - lip_bottomY)))
            {
                min_lip = Math.Abs((int)(lip_aboveY - lip_bottomY));
            }
        }

        /////////////////눈 찾기/////////////////
        private void find_eyes()
        {
            // facepoints[29] = 왼쪽눈,  facepoints[62] = 오른쪽눈, facepoints[2] = 이마중간
            eyes_X = (int)facePoints[29].X;
            eyes_Y = (int)facePoints[2].Y;
            eyes_width = (int)facePoints[62].X - (int)facePoints[29].X;
            eyes_height = (int)facePoints[5].Y - (int)facePoints[2].Y;
        }

        /////////////////코 찾기/////////////////
        private void find_nose()
        {
            noseX = facePoints[38].X;
            noseY = facePoints[38].Y;

            Canvas.SetLeft(ellipse2, facePoints[38].X - ellipse2.Width / 2);
            Canvas.SetTop(ellipse2, facePoints[38].Y - ellipse2.Height / 2);
        }

        /////////////////턱 찾기/////////////////
        private void find_chin()
        {
            chinX = facePoints[10].X;
            chinY = facePoints[10].Y;

            Canvas.SetLeft(ellipse4, facePoints[10].X - ellipse4.Width / 2);
            Canvas.SetTop(ellipse4, facePoints[10].Y - ellipse4.Height / 2);
        }

        /////////////////목 찾기/////////////////
        private System.Windows.Point find_neck(Skeleton skeleton, DepthImageFrame depthImageFrame)
        {
            Joint neckJoint = skeleton.Joints[JointType.ShoulderCenter];

            DepthImagePoint neckDepthPoint;
            neckDepthPoint = depthImageFrame.MapFromSkeletonPoint(neckJoint.Position);

            System.Windows.Point neckPoint = new System.Windows.Point((int)(image1.ActualWidth * neckDepthPoint.X
                                               / depthImageFrame.Width),
                                    (int)(image1.ActualHeight * neckDepthPoint.Y
                                               / depthImageFrame.Height));

            Canvas.SetLeft(ellipse3, neckPoint.X - ellipse1.Width / 2);
            Canvas.SetTop(ellipse3, neckPoint.Y - ellipse1.Height / 2);

            return neckPoint;
        }

        /////////////////머리 찾기/////////////////
        private System.Windows.Point find_head(Skeleton skeleton, DepthImageFrame depthImageFrame)
        {
            Joint headJoint = skeleton.Joints[JointType.Head];

            DepthImagePoint headDepthPoint;
            headDepthPoint = depthImageFrame.MapFromSkeletonPoint(headJoint.Position);

            System.Windows.Point headPoint = new System.Windows.Point((int)(image1.ActualWidth * headDepthPoint.X
                                               / depthImageFrame.Width),
                                    (int)(image1.ActualHeight * headDepthPoint.Y
                                               / depthImageFrame.Height));

            Canvas.SetLeft(ellipse1, headPoint.X - ellipse1.Width / 2);
            Canvas.SetTop(ellipse1, headPoint.Y - ellipse1.Height / 2);

            return headPoint;

        }

        /////////////////입술 표시/////////////////
        private void set_lip()
        {
            Canvas.SetLeft(rect1, lip_aboveX);
            Canvas.SetTop(rect1, lip_aboveY);

            Canvas.SetLeft(rect2, lip_bottomX);
            Canvas.SetTop(rect2, lip_bottomY);
        }

        /////////////////고개 끄덕임 검사/////////////////
        private void check_nod(Point headPoint, Point neckPoint)
        {
            ///////////////고개의 각도를 계산 ////////////////////

            lineOfHeadNose.X1 = headPoint.X;
            lineOfHeadNose.Y1 = headPoint.Y;
            lineOfHeadNose.X2 = noseX;
            lineOfHeadNose.Y2 = noseY;

            Vector headToNose = new Vector(noseX - headPoint.X, noseY - headPoint.Y);

            lineOfChinNeck.X1 = chinX;
            lineOfChinNeck.Y1 = chinY;
            lineOfChinNeck.X2 = neckPoint.X;
            lineOfChinNeck.Y2 = neckPoint.Y;

            Vector ChinToNeck = new Vector(neckPoint.X - chinX, neckPoint.Y - chinY);

            if (headToNose.Length > ChinToNeck.Length)
            {
                if (now_phase == 2)
                {
                    imgPhaseTwo.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Head/head_down.jpg"));
                }
                else if (now_phase == 3)
                {
                    imgPhaseThree.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Head/head_down.jpg"));
                }

                textBlock4.Text = string.Format("고개를 숙인상태");
                checkHeadNod.startNod = true;
            }
            else
            {
                textBlock4.Text = string.Format("고개를 든상태");
                if (checkHeadNod.startNod)
                {
                    if (now_phase == 2)
                    {
                        imgPhaseTwo.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/default.jpg"));
                    }
                    else if (now_phase == 3)
                    {
                        imgPhaseThree.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/default.jpg"));
                    }

                    Debug.WriteLine("고개듬");
                    checkHeadNod.endNod = true;
                }
            }

            //////////////// 고개 끄덕임 횟수 계산 ////////////
            if (checkHeadNod.startNod && checkHeadNod.endNod)
            {
                if (now_phase == 2)
                {
                    checkHeadNod.nodCount++;

                    checkHeadNod.startNod = false;
                    checkHeadNod.endNod = false;

                    Debug.WriteLine(string.Format("고개 {0}번 끄덕임", checkHeadNod.nodCount));
                    imgPhaseTwo.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Head/head_down.jpg"));
                }
                else if (now_phase == 3)
                {
                    // 고개각도체크 구조체 초기화
                    checkHeadNod.startNod = false;
                    checkHeadNod.endNod = false;
                    checkHeadNod.nodCount = 0;
                    checkHeadNod.nodTime = 0;
                }
            }
        }


        /////////////////눈 깜빡임 검사/////////////////
        private void check_blink(IplImage img)
        {
            const double ScaleFactor = 2.5;
            const int MinNeighbors = 1;
            CvSize MinSize = new CvSize(20, 20);

            /////// 이미지 확대 ///////////
            using (IplImage srcImg = img.Clone())
            using (IplImage dstImg = new IplImage(img.Width * 2, img.Height * 2, srcImg.Depth, srcImg.NChannels))
            {
                Cv.PyrUp(srcImg, dstImg, CvFilter.Gaussian5x5);

                ////////// 이미지 흑백화 //////////
                using (IplImage grayImg = new IplImage(dstImg.Width, dstImg.Height, BitDepth.U8, 1))
                {
                    Cv.CvtColor(dstImg, grayImg, ColorConversion.BgrToGray);
                    CvHaarClassifierCascade cascade = CvHaarClassifierCascade.FromFile("haarcascade_eye.xml");
                    CvSeq<CvAvgComp> eyes = Cv.HaarDetectObjects(grayImg, cascade, Cv.CreateMemStorage(), ScaleFactor, MinNeighbors, HaarDetectionType.DoCannyPruning, MinSize);

                    isEyesDetected = false;
                    blink_flag = true;


                    ////////// 눈 탐색 //////////
                    foreach (CvAvgComp eye in eyes.AsParallel())
                    {
                        try
                        {
                            IplImage EyeImg1 = dstImg.Clone();
                            Cv.SetImageROI(EyeImg1, eye.Rect);
                            IplImage EyeImg2 = Cv.CreateImage(eye.Rect.Size, EyeImg1.Depth, EyeImg1.NChannels);
                            Cv.Copy(EyeImg1, EyeImg2, null);
                            Cv.ResetImageROI(EyeImg1);
                            eye_templateSrc = EyeImg2.Clone();

                            ////////정면주시 여부 템플릿매칭 /////////
                            if (isTempleteMatch)
                            {
                                eye_template = EyeImg2;
                                Console.WriteLine("템플릿 저장");
                                isTempleteMatch = false;
                                startTempleteMatch = true;
                            }


                            if (startTempleteMatch && now_phase == 3)
                            {
                                check_eyeFront();
                            }


                            System.Drawing.Bitmap EyeBm = BitmapConverter.ToBitmap(EyeImg2);
                            eye_image.Source = Bitmap2BitmapSource(EyeBm);


                            blink_frame = 1;
                            blink_flag = true;

                            eye_rect = eye.Rect;
                            if (now_phase == 3)
                            {
                                BuildHist(EyeImg2);
                            }
                            isEyesDetected = true;

                        }
                        catch
                        {
                        }
                        break;
                    }

                    if (isEyesDetected == false)
                    {
                        try
                        {

                            IplImage eyeImg1 = img.Clone();
                            Cv.SetImageROI(eyeImg1, eye_rect);
                            IplImage eyeImg2 = Cv.CreateImage(eye_rect.Size, eyeImg1.Depth, eyeImg1.NChannels);
                            Cv.Copy(eyeImg1, eyeImg2, null);
                            Cv.ResetImageROI(eyeImg1);

                            System.Drawing.Bitmap EyeBm = BitmapConverter.ToBitmap(eyeImg2);
                            eye_image.Source = Bitmap2BitmapSource(EyeBm); ;

                            blink_frame = 1;

                            if (now_phase == 3)
                            {
                                BuildHist(eyeImg2);
                            }
                        }
                        catch
                        {
                        }

                    }
                }
            }

            if (blink_flag && blink_frame != 0)
            {
                blink_frame++;
                if (blink_frame > Frames)
                {

                    blink_frame = 0;
                    blink_cnt++;
                    Console.WriteLine("눈깜빡임" + blink_cnt + "회");
                }
            }

            img = null;

        }

        /////////////////정면주시 여부 검사/////////////////
        private void check_eyeFront()
        {
            CvPoint minloc, maxloc;
            Double minval, maxval;

            if (eye_templateSrc.Width < eye_template.Width || eye_templateSrc.Height < eye_template.Height)
            {
                Console.WriteLine("눈 인식안됨");
            }
            else
            {
                eye_templateRes = new IplImage(new CvSize(eye_templateSrc.Width - eye_template.Width + 1, eye_templateSrc.Height - eye_template.Height + 1), BitDepth.F32, 1);
                Cv.MatchTemplate(eye_templateSrc, eye_template, eye_templateRes, MatchTemplateMethod.SqDiffNormed);
                Cv.MinMaxLoc(eye_templateRes, out minval, out maxval, out minloc, out maxloc);

                if (minval < 0.02)
                {
                    Console.WriteLine("정면주시중");
                }
                else
                {
                    match_frame++;
                    if (match_frame > Frames)
                    {
                        match_frame = 0;
                        match_cnt++;
                        Console.WriteLine("다른곳을 보는중" + match_cnt + "초");
                        imgPhaseThree.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Eye/eye_notfront.jpg"));
                    }
                }
            }

        }

        /////////////////하품상태 검사/////////////////
        private void calc_lip()
        {

            if (lip_bottomY - lip_aboveY < min_lip * 1.8)      // 윗입술 아랫입술 떨어진 상태 검사
            {
                text2.Text = "평소상태";
                open_sec = 0;
            }
            else
            {

                open_sec++;

                if (open_flag == true)
                {
                    if (frame_sec > Frames * 60)             //1분내에 하품작동이 있었는지 검사
                        frame_sec = 0;
                    else
                        frame_sec++;
                }

                if (open_sec <= (int)(Frames * 1.5))
                {
                    text2.Text = "벌어진 상태";
                }
                else if (open_sec > (int)(Frames * 1.5))
                {
                    if (open_flag == true && frame_sec > (int)(Frames * 2))
                    {
                        text2.Text = "pase 3 로";
                        imgPhaseTwo.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Head/grey_head_yawning.jpg"));
                        ChangePhase(3);
                        Debug.WriteLine("1 졸음의 성숙단계로 넘어갑니다. ");

                        btPhaseTwo.IsChecked = false;
                        btPhaseThree.IsChecked = true;

                        Thread BeepThread = new Thread(new ThreadStart(call_PreBeep)); // 예비신호
                        BeepThread.Start();

                        return;
                    }
                    imgPhaseTwo.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Head/head_yawning.jpg"));
                    text2.Text = "하품 상태";
                    open_flag = true;
                }

            }
        }

        /////////////////비트맵을 비트맵 소스로 변환/////////////////
        public BitmapSource Bitmap2BitmapSource(System.Drawing.Bitmap bitmap)
        {
            BitmapSource bitmapSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap
            (
                bitmap.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions()
            );

            return bitmapSource;
        }


        /////////////////비트맵 소스를 비트맵으로 변환/////////////////
        private System.Drawing.Bitmap BitmapSource2Bitmap(BitmapSource bitmapsource)
        {
            System.Drawing.Bitmap bitmap;
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream); bitmap = new System.Drawing.Bitmap(outStream);
            }
            return bitmap;
        }

        /////////////////고개 각도 표시/////////////////
        public void announce_angle()
        {
            lineOfHeadNose.Stroke = System.Windows.Media.Brushes.LightSteelBlue;
            lineOfHeadNose.HorizontalAlignment = HorizontalAlignment.Left;
            lineOfHeadNose.VerticalAlignment = VerticalAlignment.Bottom;
            lineOfHeadNose.StrokeThickness = 2;
            canvas1.Children.Add(lineOfHeadNose);

            lineOfChinNeck.Stroke = System.Windows.Media.Brushes.LightCoral;
            lineOfChinNeck.HorizontalAlignment = HorizontalAlignment.Left;
            lineOfChinNeck.VerticalAlignment = VerticalAlignment.Bottom;
            lineOfChinNeck.StrokeThickness = 2;
            canvas1.Children.Add(lineOfChinNeck);
        }

        /////////////////gps 설정/////////////////
        public void announce_gps()
        {
            dTimer = new DispatcherTimer();
            pastGeo = new double[1, 2];
            dTimer.Interval = TimeSpan.FromMilliseconds(1000);
            dTimer.Tick += dTimer_Tick;
        }
        #endregion 키넥트 소스

        #region 신호
        /////////////////예비신호 알림/////////////////
        private void call_PreBeep()
        {
            for (int i = 1; i < 10; i++)
            {
                Beep(256, 1000);
                Thread.Sleep(1000);
            }
        }

        /////////////////본 경고음 알림/////////////////
        private void call_OverBeep()
        {
            for (int i = 1; i < 40; i++)
            {
                Beep(512, 500);
                Thread.Sleep(500);
            }
        }
        #endregion

        #region 히스토그램

        IplImage DstHist;

        public IplImage BuildHist(IplImage src)
        {
            const int histSize = 32;
            float[] range0 = { 10, 72 };
            float[][] ranges = { range0 };

            // 화상의 읽기
            IplImage srcImg = new IplImage(src.Size, BitDepth.U8, 1);
            IplImage dstImg = new IplImage(src.Size, BitDepth.U8, 1);
            IplImage histImg = new IplImage(new CvSize(80, 80), BitDepth.U8, 1);
            CvHistogram hist = new CvHistogram(new int[] { histSize }, HistogramFormat.Array, ranges, true);

            src.CvtColor(srcImg, ColorConversion.BgrToGray);
            srcImg.Copy(dstImg);


            // 히스토그램 그리기
            CalcHist(dstImg, hist);
            DrawHist(histImg, hist, histSize, src);
            // 윈도우에 표시
            DstHist = histImg.Clone();

            System.Drawing.Bitmap histogram_BM = BitmapConverter.ToBitmap(histImg);
            //            pctLeftEye.Image = histogram_BM; 히스토그램 그림 만든거

            dstImg.Zero();
            histImg.Zero();

            return DstHist;

        }

        private static void CalcHist(IplImage img, CvHistogram hist) /// 히스토그램 계산
        {
            hist.Calc(img);
            float minValue, maxValue;
            hist.GetMinMaxValue(out minValue, out maxValue);
            Cv.Scale(hist.Bins, hist.Bins, ((double)img.Height) / maxValue, 0);

        }


        private void DrawHist(IplImage img, CvHistogram hist, int histSize, IplImage tmImg) // 히스토그램 그리기()
        {
            img.Set(CvColor.White);
            int binW = Cv.Round((double)img.Width / histSize);
            eye_frames = (eye_frames + 1) % 60;
            eye_hist[eye_frames] = 0;
            for (int i = 0; i < histSize; i++)
            {
                eye_hist[eye_frames] += i * (int)hist.Bins[i];
            }
            //2초마다 변화 평균 계산
            if (eye_frames % 30 == 0)
            {
                int average = 0;
                if (eye_frames == 0)
                {
                    average += eye_hist[0];
                    for (int i = 31; i < 60; i++)
                    {
                        average += eye_hist[i];
                    }
                    average_cnt++;
                }
                else
                {
                    for (int i = 1; i < 31; i++)
                    {
                        average += eye_hist[i];
                    }
                    average_cnt++;
                }
                average /= 30;

                if (average > 6000)
                    eye_average[average_cnt] = average;

                if (average_cnt < 3)
                    return;

                Console.WriteLine(average);

                if (average_min > average)
                    average_min = average;

                if (average_max < average)
                    average_max = average;

                average_cnt = average_cnt % 60;
                int test = 0;

                if (average_cnt < 6)
                    return;

                /*for (int i = 0; i < 3; i++) // 여기서 지속여부 판단
                {
                    if (eye_average[(average_cnt - i) % 60] > average_min + 1000 || eye_average[(average_cnt - i) % 60] < average_max - 1000) //눈 감김이 일정이상 감김이 지속
                    {
                      test++;
                    }
                }
                if (eye_average[average_cnt] > average_min + 1500 || eye_average[average_cnt] < average_max - 1500)// 여기서 지속여부 판단
                    test++;
                 */

                //////////////자동 템플릿 이미지 저장 //////////////
                if (average > 9000 && average < 15000 && !startTempleteMatch)
                {
                    eye_template = tmImg;
                    startTempleteMatch = true;
                    Debug.WriteLine("자동 템플릿 저장");
                }
                
                ///////////////눈 많이 감아짐 지속 판단 //////////////
                for (int i = 0; i < 3; i++)
                {
                    if (eye_average[average_cnt] < 50000)
                        test++;
                }
                
                if (test > 2)
                {

                    Thread BeepThread = new Thread(new ThreadStart(call_PreBeep));
                    BeepThread.Start();

                    textblock5.Text = "눈이 많이 잠겨짐";
                    imgPhaseThree.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/Eye/grey_eye_tooclose.jpg"));
                    ChangePhase(4);
                    Debug.WriteLine(" 졸음의 실행 ");
                }

            }
        }

        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            isTempleteMatch = true;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

        }

        private void button5_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                nui.ElevationAngle = int.Parse(textBox1.Text);
            }
            catch (Exception ex)
            {

            }
        }


        private void grdTitleBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btMin_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        #region 시작 버튼 이미지 변경
        private void imgRun_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (isRunning)
            {
                imgRun.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/start_01.png"));
                nui.Stop();
                image1.Source = null;
                isRunning = false;
            }
            else
            {
                imgRun.Source = new BitmapImage(new Uri(@"pack://application:,,,/Assets/stop_01.png"));
                isRunning = true;
                Kinect_Start();
            }
        }


        #endregion

        private void ChangePhase(int num)
        {
            if (num < 2)
            {
                imgDriveChk.Visibility = Visibility.Hidden;
                imgTwoChk.Visibility = Visibility.Hidden;

                btDriveChk.IsChecked = true;
                btPhaseTwo.IsChecked = false;
                btPhaseThree.IsChecked = false;

                now_phase = 1;
            }
            else if (num == 2)
            {
                imgDriveChk.Visibility = Visibility.Visible;
                imgTwoChk.Visibility = Visibility.Hidden;

                btDriveChk.IsChecked = false;
                btPhaseTwo.IsChecked = true;
                btPhaseThree.IsChecked = false;

                now_phase = 2;
            }
            else if (num == 3)
            {
                imgDriveChk.Visibility = Visibility.Visible;
                imgTwoChk.Visibility = Visibility.Visible;

                btDriveChk.IsChecked = false;
                btPhaseTwo.IsChecked = false;
                btPhaseThree.IsChecked = true;

                now_phase = 3;
            }
            else if (num == 4)
            {
                imgThreeChk.Visibility = Visibility.Visible;

                now_phase = 4;
            }

        }
    }
}
